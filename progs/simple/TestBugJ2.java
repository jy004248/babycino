class Main {
    public static void main(String[] args) {
        int x = 5;
        int y = 3;

        // Bug J2: Instead of assigning x + y to x, assign y to x
        x = x + y;
        System.out.println("x = " + x); // should print "x = 8"

        x = y;
        System.out.println("x = " + x); // should print "x = 3"
    }
}
