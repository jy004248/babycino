public class Main {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        int x = 5;
        int y = 5;

        // comparing two boolean values
        if (a == b) {
            System.out.println("a and b are equal.");
        } else {
            System.out.println("a and b are not equal.");
        }

        // comparing two int values
        if (x == y) {
            System.out.println("x and y are equal.");
        } else {
            System.out.println("x and y are not equal.");
        }

        // comparing a boolean value with an int value
        int boolToInt = a ? 1 : 0;
        if (boolToInt == x) {
            System.out.println("a and x are equal.");
        } else {
            System.out.println("a and x are not equal.");
        }
    }
}
