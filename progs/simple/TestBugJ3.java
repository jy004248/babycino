class Main {
    public static void main(String[] args) {
        int x = 5;
        int y = 3;

        // Bug J3: Do not modify x at all
        int result = x + y;
        System.out.println("x = " + x); // should print "x = 5"
        System.out.println("result = " + result); // should print "result = 8"
    }
}
