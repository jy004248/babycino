public class Main {
    public static void main(String[] args) {
        int x = 5;
        int y = 3;
        boolean z = true;

        // Example of correct use of the += operator
        x += y;
        System.out.println("x = " + x); // prints "x = 8"

        // Bug J1: Allow y to be a boolean expression
        x += (z ? 1 : 0);
        System.out.println("x = " + x); // prints "x = 9"

        // Bug J2: Instead of assigning x + y to x, assign y to x
        x = y;
        System.out.println("x = " + x); // prints "x = 3"

        // Bug J3: Do not modify x at all
        int result = x + y;
        System.out.println("x = " + x); // prints "x = 3"
        System.out.println("result = " + result); // prints "result = 6"
    }
}
