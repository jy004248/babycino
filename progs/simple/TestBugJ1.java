class Main {
    public static void main(String[] args) {
        int x = 5;
        int y = 3;
        boolean z = true;

        // Bug J1: Allow y to be a boolean expression
        x += (y < 4);
        System.out.println("x = " + x); // should print "x = 6"
    }
}
