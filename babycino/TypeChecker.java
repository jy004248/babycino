package babycino;

import org.antlr.v4.runtime.ParserRuleContext;
import java.util.Stack;
import java.util.ArrayList;
import java.util.Set;
import java.util.Map;

// Check the types of all expressions in the parse tree.
// Also, record the static types of the receivers of method calls for later.
public class TypeChecker extends MiniJavaBaseListener {

    // Symbol Table for the program being type-checked.
    private SymbolTable sym;
    // Class currently being type-checked.
    private Class current;
    // Method currently being type-checked.
    private Method method;
    // Flag: Have any errors occurred so far?
    private boolean errors;

    // Stack of unprocessed types, corresponding to checked subexpressions.
    private Stack<Type> types;
    
    public TypeChecker(SymbolTable sym) {
        this.sym = sym;
        this.method = null;
        this.errors = false;
        this.types = new Stack<Type>();
    }

    // ------------------------------------------------------------------------
    // Track what the current class/method is.

    @Override
    public void enterMainClass(MiniJavaParser.MainClassContext ctx) {
        this.current = sym.get(ctx.identifier(0).getText());
        // Set a dummy method with no variables to avoid null-pointer errors later.
        this.method = new Method("main", null, this.current, null);
    }

    @Override
    public void exitMainClass(MiniJavaParser.MainClassContext ctx) {
        this.current = null;

        // It is a fatal error if somehow not all types on the stack are used.
        if (!this.types.isEmpty()) {
            System.err.println("Internal error: not all types consumed during type-checking.");
            System.exit(1);
        }
    }
    
    @Override
    public void enterClassDeclaration(MiniJavaParser.ClassDeclarationContext ctx) {
        this.current = this.sym.get(ctx.identifier(0).getText());
    }
    
    @Override
    public void exitClassDeclaration(MiniJavaParser.ClassDeclarationContext ctx) {
        this.current = null;
    }

    @Override
    public void enterMethodDeclaration(MiniJavaParser.MethodDeclarationContext ctx) {
        this.method = this.current.getOwnMethod(ctx.identifier(0).getText());
    }

    @Override
    public void exitMethodDeclaration(MiniJavaParser.MethodDeclarationContext ctx) {
        Type t = this.types.pop();
        Type ret = this.method.getReturnType();
        check(ret.compatibleWith(t), ctx, "Return type of " + this.method.getQualifiedName() +
            " expected to be compatible with " + ret + "; actual type: " + t);
        this.method = null;

        // It is a fatal error if somehow not all types on the stack are used.
        if (!this.types.isEmpty()) {
            System.err.println("Internal error: not all types consumed during type-checking.");
            System.exit(1);
        }
    }

    // ------------------------------------------------------------------------
    // When leaving an expression or statement:
    // firstly, pop the types of any subexpressions off the stack and check them;
    // secondly, for expressions, push the type of the expression onto the stack.

    // Statements:

    @Override
    public void exitStmtIf(MiniJavaParser.StmtIfContext ctx) {
        Type t = this.types.pop();
        check(t.isBoolean(), ctx, "Expected condition of if statement to be boolean; actual type: " + t);
    }

    @Override
    public void exitStmtWhile(MiniJavaParser.StmtWhileContext ctx) {
        Type t = this.types.pop();
        check(t.isBoolean(), ctx, "Expected condition of while statement to be boolean; actual type
@Override
public void exitExpConstTrue(MiniJavaParser.ExpConstTrueContext ctx) {
    this.types.push(new Type(Kind.BOOLEAN));
}

@Override
public void exitExpArrayLength(MiniJavaParser.ExpArrayLengthContext ctx) {
    Type t = this.types.pop();
    this.check(t.isIntArray(), ctx, "Expected length to be applied to expression of type int[]; actual type: " + t);
    this.types.push(new Type(Kind.INT));
}

@Override
public void exitExpBinOp(MiniJavaParser.ExpBinOpContext ctx) {
    Type rhs = this.types.pop();
    Type lhs = this.types.pop();
    String op = ctx.getChild(1).getText();

    if (op.equals("&&") || op.equals("||")) {
        this.check(lhs.isBoolean() && rhs.isBoolean(), ctx, "Operator " + op + " expected boolean arguments; actual types: " + lhs + ", " + rhs);
        this.types.push(new Type(Kind.BOOLEAN));
    } else if (op.equals("<") || op.equals("<=") || op.equals(">") || op.equals(">=")) {
        this.check(lhs.isInt() && rhs.isInt(), ctx, "Operator " + op + " expected int arguments; actual types: " + lhs + ", " + rhs);
        this.types.push(new Type(Kind.BOOLEAN));
    } else {
        this.check(lhs.compatibleWith(rhs), ctx, "Operator " + op + " expected compatible arguments; actual types: " + lhs + ", " + rhs);
        // Push the type of the result.
        if (lhs.isInt() && rhs.isInt()) {
            this.types.push(new Type(Kind.INT));
        } else {
            this.types.push(new Type(Kind.BOOLEAN));
        }
    }
}

@Override
public void exitExpConstFalse(MiniJavaParser.ExpConstFalseContext ctx) {
    this.types.push(new Type(Kind.BOOLEAN));
}

@Override
public void exitExpIdentifier(MiniJavaParser.ExpIdentifierContext ctx) {
    Type t = this.identifierType(ctx.identifier());
    this.types.push(t);
}

@Override
public void exitExpMethodCall(MiniJavaParser.ExpMethodCallContext ctx) {
    Type[] argTypes = new Type[ctx.expression().size()];
    for (int i = 0; i < argTypes.length; i++) {
        argTypes[argTypes.length - i - 1] = this.types.pop();
    }

    Type receiverType = this.types.pop();
    if (receiverType.isNull()) {
        // It's an instance method call on a null reference, which is a runtime error.
        this.errors = true;
        System.err.println("Runtime error: instance method call on null reference in " + ctx.getText());
        this.types.push(new Type(Kind.ERROR));
        return;
    }

    if (!receiverType.isClass()) {
        this.errors = true;
        System.err.println("Expected receiver of method call to be a class; actual type: " + receiverType + " in " + ctx.getText());
        this.types.push(new Type(Kind.ERROR));
        return;
    }

    Method m = receiverType.getMethod(ctx.identifier().getText(), argTypes);
    if (m == null) {
        this.errors = true;
        System.err.println("Method not found: " + receiverType.getQualifiedName() + "." + ctx.identifier().getText() + "(" + Type.join(argTypes) + ") in " + ctx.getText());
        this.types.push(new Type(Kind.ERROR));
        return;
    }

    this.types.push(m.getReturnType());
}

@Override
public void exitExpIdentifier(MiniJavaParser.ExpIdentifierContext ctx) {
Type t = this.identifierType(ctx.identifier());
this.types.push(t);
}

@Override
public void exitExpThis(MiniJavaParser.ExpThisContext ctx) {
this.types.push(new Type(this.current));
}

@Override
public void exitExpNewIntArray(MiniJavaParser.ExpNewIntArrayContext ctx) {
Type t = this.types.pop();
this.check(t.isInt(), ctx, "Array size expression must have type int; actual type: " + t);
this.types.push(new Type(Kind.INT_ARRAY));
}

@Override
public void exitExpNewObject(MiniJavaParser.ExpNewObjectContext ctx) {
this.types.push(new Type(this.sym.get(ctx.identifier().getText())));
}

@Override
public void exitExpMethodCall(MiniJavaParser.ExpMethodCallContext ctx) {
ArrayList<Type> argTypes = new ArrayList<Type>();
for (MiniJavaParser.ExpressionContext e : ctx.expression()) {
argTypes.add(this.types.pop());
}
Type recvType = this.types.pop();

// For the purposes of the static type checker, assume that any method called on an
// array is the same as calling it on an instance of the Array class.
if (recvType.isArray()) {
    recvType = new Type(this.sym.get("Array"));
}

// Find the method with the given name that is applicable to the given receiver and arguments.
Method method = recvType.getMethod(ctx.identifier().getText(), argTypes);
check(method != null, ctx, "No applicable method found with name " + ctx.identifier().getText() +
        " and argument types " + argTypes + " in class " + recvType.getClassName());

// Save the static type of the receiver for later.
ctx.staticType = recvType;

// Push the return type of the method onto the type stack.
this.types.push(method.getReturnType());

}

// ------------------------------------------------------------------------
// Helper methods.

// Given a parse-tree Identifier node, determine its type.
private Type identifierType(MiniJavaParser.IdentifierContext id) {
if (id.getText().equals("this")) {
return new Type(this.current);
} else if (this.method != null && this.method.hasVariable(id.getText())) {
return this.method.getVariableType(id.getText());
} else if (this.current.hasField(id.getText())) {
return this.current.getFieldType(id.getText());
} else {
return new Type(this.sym.get(id.getText()));
}
}

// If cond is false, print an error message to System.err and set the errors flag.
private void check(boolean cond, ParserRuleContext ctx, String message) {
if (!cond) {
System.err.println("Type error: " + message);
this.errors = true;
}
}

// Get the flag indicating whether errors have occurred so far.
public boolean getErrors() {
return this.errors;
}

// Get the static types of the receivers of all method calls encountered so far.
// (The keys of the returned map are parse-tree nodes.)
public Map<ParserRuleContext, Type> getStaticTypes() {
return StaticTypeCollector.getStaticTypes();
}

}

// ------------------------------------------------------------------------

// Record the static types of the receivers of method calls for later.
class StaticTypeCollector extends MiniJavaBaseListener {
// Map from parse-tree nodes to static types.
private static Map<ParserRuleContext, Type> staticTypes;
// Stack of unprocessed types, corresponding to checked subexpressions.
private Stack<Type> types;

static {
    StaticTypeCollector.staticTypes = new IdentityHashMap<

@Override
public void exitExpNewObject(MiniJavaParser.ExpNewObjectContext ctx) {
String className = ctx.identifier().getText();
Class c = this.sym.get(className);
this.types.push(new Type(c));
}

@Override
public void exitExpNewArray(MiniJavaParser.ExpNewArrayContext ctx) {
Type t = this.types.pop();
this.check(t.isInt(), ctx, "Expected array size to be an int; actual type: " + t);
this.types.push(new Type(Kind.INT_ARRAY));
}

@Override
public void exitExpNot(MiniJavaParser.ExpNotContext ctx) {
Type t = this.types.pop();
this.check(t.isBoolean(), ctx, "Expected operand of ! to be a boolean; actual type: " + t);
this.types.push(new Type(Kind.BOOLEAN));
}

// ------------------------------------------------------------------------
// Helper methods.

// Check that a type is as expected. If not, report an error.
private void check(boolean condition, ParserRuleContext ctx, String msg) {
if (!condition) {
this.errors = true;
System.err.println(msg + " (line " + ctx.getStart().getLine() + ")");
}
}

// Determine the type of an identifier by looking it up in the symbol table.
private Type identifierType(MiniJavaParser.IdentifierContext ctx) {
String name = ctx.getText();

// Check local variable table for this method.
Variable var = this.method.getVariable(name);
if (var != null) {
    return var.getType();
}

// Check fields of this class and its superclasses.
Class c = this.current;
while (c != null) {
    var = c.getField(name);
    if (var != null) {
        return var.getType();
    }
    c = c.getSuperClass();
}

// Identifier not found.
this.errors = true;
System.err.println("Identifier " + name + " not found (line " + ctx.getStart().getLine() + ")");
return null;
}

// ------------------------------------------------------------------------
// Store the static types of receiver expressions of method calls.

private Map<MiniJavaParser.MethodCallContext, Type> receiverTypes;

@Override
public void enterMethodCall(MiniJavaParser.MethodCallContext ctx) {
// Record the type of the receiver expression.
this.receiverTypes.put(ctx, this.types.pop());
}

// ------------------------------------------------------------------------
// Check method call arguments
// ------------------------------------------------------------------------
// Helper methods
// Look up the type of an identifier in the symbol table.
// Report an error if the identifier is not defined.
private Type identifierType(MiniJavaParser.IdentifierContext id) {
    String name = id.getText();
    Type t = this.sym.get(name);
    this.check(t != null, id, "Undefined identifier: " + name);
    return t;
}

// Check whether a type is a subtype of another type, or whether two types are the same.
private void check(boolean condition, ParserRuleContext ctx, String message) {
    if (!condition) {
        this.errors = true;
        System.err.println(ctx.getStart().getLine() + ":" + ctx.getStart().getCharPositionInLine() + ": " + message);
    }
}

// ------------------------------------------------------------------------
// Methods for recording the types of method call receivers.

// Record the type of the receiver of a method call.
@Override
public void enterExpDot(MiniJavaParser.ExpDotContext ctx) {
    // We only need to record the type if the method is actually defined in the class hierarchy.
    if (this.sym.hasMethod(ctx.identifier(1).getText())) {
        Type t = this.types.pop();
        this.method.recordReceiverType(t);
    }
}

// When we exit a method call expression with an explicit receiver, we just discard the recorded type.
@Override
public void exitExpMethodCall(MiniJavaParser.ExpMethodCallContext ctx) {
    if (ctx.exp() != null) {
        this.method.popReceiverType();
    }
}

// When we exit a method call expression with an implicit "this" receiver, we record the type of the current class.
@Override
public void exitExpMethodCallImplicit(MiniJavaParser.ExpMethodCallImplicitContext ctx) {
    this.method.recordReceiverType(this.current.getType());
}

// ------------------------------------------------------------------------
// Getter methods for use by the driver program.

// Return true if any errors occurred during type-checking.
public boolean hasErrors() {
    return this.errors;
}

// Return the static types of the receivers of all method calls.
public Map<Method, Type> getReceiverTypes() {
    return this.sym.getReceiverTypes();
}

// Return the set of classes that are reachable from the main class.
public Set<Class> getReachableClasses() {
    return this.sym.getReachableClasses();
}
}
